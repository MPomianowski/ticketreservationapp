package com.pomianowski.ticketup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.pomianowski.ticketup.repository")
@SpringBootApplication
public class TicketReservationApp {

	public static void main(String[] args) {
		SpringApplication.run(TicketReservationApp.class, args);
	}

}

