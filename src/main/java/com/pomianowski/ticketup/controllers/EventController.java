package com.pomianowski.ticketup.controllers;

import com.pomianowski.ticketup.model.Event;
import com.pomianowski.ticketup.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/rest/event")
public class EventController {

    @Autowired
    private EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping
    public void register(@RequestBody Event event) {
        eventService.create(event);
    }

}
