package com.pomianowski.ticketup.controllers;

import com.pomianowski.ticketup.model.Reservation;
import com.pomianowski.ticketup.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/rest/reservation")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping
    public List<Reservation> getAll() {
        return reservationService.findAll();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value = "/{userId}")
    public List<Reservation> getAllByUserId(@PathVariable final Integer userId) {
        return reservationService.findByUserId(userId);
    }

	@PostMapping
	public void save(@RequestBody final Reservation reservation) {
		reservationService.save(reservation);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/delete/{id}")
    public void delete(@PathVariable final Integer id) {
        reservationService.delete(id);
    }
}
