package com.pomianowski.ticketup.controllers;

import com.pomianowski.ticketup.dto.UserDto;
import com.pomianowski.ticketup.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/rest/user")
public class UserController {

    @Autowired
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public void register(@RequestBody UserDto userDto) {
        userService.register(userDto);
    }

}
