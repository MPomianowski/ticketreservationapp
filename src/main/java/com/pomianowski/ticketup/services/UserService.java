package com.pomianowski.ticketup.services;

import com.pomianowski.ticketup.dto.UserDto;
import com.pomianowski.ticketup.model.Role;
import com.pomianowski.ticketup.model.User;
import com.pomianowski.ticketup.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UsersRepository usersRepository;

    public UserService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public void register(UserDto userDto){
        String encodedPassword = bCryptPasswordEncoder.encode(userDto.getPassword());
        userDto.setPassword(encodedPassword);
        Set<Role> roles = new HashSet<>();
        Role role = new Role(  2, "USER");
        roles.add(role);
        User user = new User(userDto.getName(), userDto.getLastName(), userDto.getEmail(), userDto.getPassword(), roles);
        usersRepository.save(user);
    }

}
