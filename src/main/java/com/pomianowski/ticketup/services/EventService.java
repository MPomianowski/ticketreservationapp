package com.pomianowski.ticketup.services;

import com.pomianowski.ticketup.model.Event;
import com.pomianowski.ticketup.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public void create(Event event){
        eventRepository.save(event);
    }

}
