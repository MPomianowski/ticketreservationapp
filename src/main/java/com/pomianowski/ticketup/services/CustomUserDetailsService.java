package com.pomianowski.ticketup.services;

import com.pomianowski.ticketup.model.CustomUserDetails;
import com.pomianowski.ticketup.model.User;
import com.pomianowski.ticketup.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = usersRepository.findByName(username);
        optionalUser.orElseThrow(() -> new UsernameNotFoundException("User with this username not found"));
        return optionalUser.map(user -> new CustomUserDetails(user)).get();
    }
}
