package com.pomianowski.ticketup.services;

import com.pomianowski.ticketup.model.Reservation;
import com.pomianowski.ticketup.repository.ReservationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {

    public ReservationService(ReservationsRepository reservationsRepository) {
        this.reservationsRepository = reservationsRepository;
    }

    @Autowired
    private ReservationsRepository reservationsRepository;

    public List<Reservation> findAll() {
        return reservationsRepository.findAll();
    }

    public Optional<Reservation> findById(int id) {
		return reservationsRepository.findById(id);
	}

    public void save(Reservation reservation) {
        reservation.setCreationDate(new Date());
        reservationsRepository.save(reservation);
    }

    public void delete(Integer id) {
        Optional<Reservation> reservation = reservationsRepository.findById(id);
        if(reservation.isPresent()) {
            reservationsRepository.delete(reservation.get());
        }
    }

    public  List<Reservation> findByUserId(Integer userId) {
        return reservationsRepository.findByUserId(userId);
    }
}
