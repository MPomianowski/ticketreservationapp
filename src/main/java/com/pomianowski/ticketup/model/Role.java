package com.pomianowski.ticketup.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private int roleId;

    @Column(name = "role", unique=true)
    private String role;

    public Role(){

    }

    public Role(int roleId, String role) {
        this.roleId = roleId;
        this.role = role;
    }
}
