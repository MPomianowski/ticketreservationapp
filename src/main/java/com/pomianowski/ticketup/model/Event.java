package com.pomianowski.ticketup.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;
    @Column(name = "event_date")
    private Date eventDate;
    @Column(name = "regular_tickets_quantity")
    private Integer regularTicketsQuantity;
    @Column(name = "special_tickets_quantity")
    private Integer SpecialTicketsQuantity;
    @Column(name = "regular_ticket_price")
    private BigDecimal regularTicketPrice;
    @Column(name = "special_ticket_price")
    private BigDecimal SpecialTicketPrice;
    @Column(name = "are_special_tickets_sold_out")
    private Boolean areSpecialTicketsSoldOut;
    @Column(name = "are_regular_tickets_sold_out")
    private Boolean areRegularTicketsSoldOut;
}
