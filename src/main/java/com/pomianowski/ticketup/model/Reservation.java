package com.pomianowski.ticketup.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Reservation {

    @Id
    @GeneratedValue
    private Integer id;
    private Integer eventId;
    private Integer userId;
    private Boolean isSpecialTicket;
    private Date creationDate;
    private Boolean isTicketPaid;

}
