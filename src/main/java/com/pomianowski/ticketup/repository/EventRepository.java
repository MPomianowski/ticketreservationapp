package com.pomianowski.ticketup.repository;

import com.pomianowski.ticketup.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EventRepository extends JpaRepository<Event, Integer> {
    Optional<Event> findByName(String eventName);
}
