package com.pomianowski.ticketup.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ReservationDto {

    private Integer eventId;
    private Integer userId;
    private Boolean isSpecialTicket;
    private Date creationDate;
    private Boolean isTicketPaid;

    public ReservationDto(Integer eventId, Integer userId, Boolean isSpecialTicket, Date creationDate, Boolean isTicketPaid) {
        this.eventId = eventId;
        this.userId = userId;
        this.isSpecialTicket = isSpecialTicket;
        this.creationDate = creationDate;
        this.isTicketPaid = isTicketPaid;
    }
}
