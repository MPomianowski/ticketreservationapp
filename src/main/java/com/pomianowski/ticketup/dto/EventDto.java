package com.pomianowski.ticketup.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class EventDto {

    private String name;
    private Date eventDate;
    private Integer regularTicketsQuantity;
    private Integer SpecialTicketsQuantity;
    private BigDecimal regularTicketPrice;
    private BigDecimal SpecialTicketPrice;

    public EventDto(String name, Date eventDate, Integer regularTicketsQuantity, Integer specialTicketsQuantity, BigDecimal regularTicketPrice, BigDecimal specialTicketPrice) {
        this.name = name;
        this.eventDate = eventDate;
        this.regularTicketsQuantity = regularTicketsQuantity;
        SpecialTicketsQuantity = specialTicketsQuantity;
        this.regularTicketPrice = regularTicketPrice;
        SpecialTicketPrice = specialTicketPrice;
    }
}
