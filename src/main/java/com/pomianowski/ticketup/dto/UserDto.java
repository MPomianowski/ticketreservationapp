package com.pomianowski.ticketup.dto;

import lombok.Data;

@Data
public class UserDto {

    private String name;
    private String lastName;
    private String email;
    private String password;

    public UserDto(String name, String lastName, String email, String password) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }
}
